import * as path from "path";
import * as webpack from "webpack";
// in case you run into any typescript error when configuring
import "webpack-dev-server";
import HTMLWebpackPlugin from "html-webpack-plugin";

const config: (env, argv) => webpack.Configuration = (env, argv) => {
  const mode = argv.mode === "production" ? "production" : "development";
  const devtool = mode === "production" ? "source-map" : "eval";
  const optimization: webpack.Configuration["optimization"] = env.WEBPACK_SERVE
    ? { splitChunks: { chunks: "all" }, runtimeChunk: "single" }
    : undefined;
  const filename =
    argv.mode === "production" ? "js/[name].[contenthash].js" : "[name].js";

  return {
    mode,
    devtool,
    entry: "./pages/index.tsx",
    output: {
      path: path.resolve(process.cwd(), "dist"),
      filename,
      clean: true,
    },
    plugins: [
      new HTMLWebpackPlugin({
        template: path.resolve(process.cwd(), "./assets/index.html"),
      }),
    ],
    module: { rules: [{ test: /\.tsx?$/, loader: "swc-loader" }] },
    performance: { hints: false },
    devServer: { hot: true },
    stats: "minimal",
    optimization,
  };
};
export default config;
